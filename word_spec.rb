require_relative 'word-test'

describe 'Word' do
  test_word = Word.new
	test_word.word = 'benjamin'

	context 'without running the get_sequences method' do
		it 'should return an empty sequences array' do
			expect(test_word.sequences).to eq([])
		end
	end

	context 'get_sequences method' do
		it 'should return an array' do
      expect(test_word.get_sequences).to be_an Array
		end
	end

	context 'each result of the get_sequences array' do
		it 'should be 4 characters long' do
			expect(test_word.get_sequences[0][0].length).to eq(4)
		end
	end

end