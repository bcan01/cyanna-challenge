require 'open-uri'
url = 'https://s3.amazonaws.com/cyanna-it/misc/dictionary.txt'
result = open(url)

class Word
	attr_accessor :word, :sequences
	def initialize
		@word = nil
		@sequences = []
	end
	def get_sequences
		@sequences = @word.scan(/(?=(....))/)
	end
	def print(filename,input)
		open(filename, 'a') { |f| 
			f << input
		}
	end
end

File.foreach(result) do |line|
  each_word = Word.new
  each_word.word = line
  each_word.get_sequences

  each_word.sequences.each do |sequence|
	  each_word.print('sequences.txt', "#{sequence[0]}\n")
	  each_word.print('words.txt', each_word.word)
	end
end

